package main

import (
	"fmt"
	"log"
	"net/http"
)

var URL string = "https://github.com"

func main() {
	fmt.Println("hi")
	http.Handle("/", http.RedirectHandler(URL, http.StatusMovedPermanently))
	log.Fatal(http.ListenAndServe(":9000", nil))
}
